package com.example.cinemaapp.data

//Random favorite movies
fun favoriteMovies() : List<Movie> {
    return listOf(
        Movie(1, "John Wick", "American neo-noir action thriller film directed by Chad Stahelski, in his directorial debut, and written by Derek Kolstad.", "2014", "https://www.imdb.com/title/tt2911666/", "action"),
        Movie(2, "Focus", "In the midst of veteran con man Nicky's latest scheme, a woman from his past - now an accomplished femme fatale - shows up and throws his plans for a loop.", "2015", "https://www.imdb.com/title/tt2381941/", "drama"),
        Movie(3, "Taken", "A retired CIA agent travels across Europe and relies on his old skills to save his estranged daughter, who has been kidnapped while on a trip to Paris.", "2008", "https://www.imdb.com/title/tt0936501/", "action"),
        Movie(4, "Passengers", "A malfunction in a sleeping pod on a spacecraft traveling to a distant colony planet wakes one passenger 90 years early.", "2016", "https://www.imdb.com/title/tt1355644/", "romance")
    )
}

//Random recent movies
fun recentMovies() : List<Movie> {
    return listOf(
        Movie(1,"The Contractor", "A discharged U.S. Special Forces sergeant, James Harper, risks everything for his family when he joins a private contracting organization.","01.04.2022.","https://www.imdb.com/title/tt10323676/","thriller"),
        Movie(2, "Spider-Man: No Way Home", "With Spider-Man's identity now revealed, Peter asks Doctor Strange for help. When a spell goes wrong, dangerous foes from other worlds start to appear, forcing Peter to discover what it truly means to be Spider-Man.", "2021", "https://www.imdb.com/title/tt10872600/", "action"),
        Movie(3, "Toma", "Biopic about Toma Zdravkovic, the man who is remembered not only for his songs and the unique way he sang them, but also as a bohemian, both in his behavior and his soul.", "2021", "https://www.imdb.com/title/tt8737152/", "biography"),
        Movie(4, "Juzni vetar", "A young member of a gang in Belgrade puts his family in danger when he angers a mafia boss.", "2020", "https://www.imdb.com/title/tt10952900/", "crime")
    )
}