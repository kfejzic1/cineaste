package com.example.cinemaapp.data

//Class for getting data and transfering it to ViewModel Classes

object MovieRepository {
    fun getFavoriteMovies() : List<Movie> {
        return favoriteMovies()
    }

    fun getRecentMovies() : List<Movie> {
        return recentMovies()
    }
}