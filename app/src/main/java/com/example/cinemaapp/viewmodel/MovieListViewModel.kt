package com.example.cinemaapp.viewmodel

import com.example.cinemaapp.data.Movie
import com.example.cinemaapp.data.MovieRepository

class MovieListViewModel {
    fun getFavoriteMovies() : List<Movie> {
        return MovieRepository.getFavoriteMovies()
    }

    fun getRecentMovies() : List<Movie> {
        return MovieRepository.getRecentMovies()
    }
}